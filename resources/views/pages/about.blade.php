@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="about-title">À propos de iNotreDame</h3>

    <p class="about-content">Afin de vous permettre d'accéder aux services proposés par votre établissement, ce dernier a mis en place cette plateforme intranet.</p>
    <p class="about-content">Voici les liens des outils à votre disposition, que vous pourrez consulter :</p>

    <ul>
        <li class="about-list">
            <a href="/files" class="about-link">Logiciels à télécharger</a>
            <p>Sur cette page, vous retrouverez une liste des logiciels scolaires présents sur ce site, avec des liens pour les télécharger.</p>
        </li>

        <li class="about-list">
            <a href="/agenda" class="about-link">Agenda</a>
            <p>Cet outil vous tiendra informé·e des dates à retenir.</p>
        </li>

        <li class="about-list">
            <a href="http://wiki.notre-dame-reze.fr/doku.php" class="about-link" target="_blank">Wiki</a>
            <p>Le wiki de l'ensemble scolaire Notre-Dame de Rezé.</p>
        </li>
        <li class="about-list">
            <a href="https://login.microsoftonline.com/fr" class="about-link" target="_blank">Office 365</a>
            <p>Ce lien vous dirigera vers la plateforme Office 365 du lycée et ses applications.</p>
        </li>
        <li class="about-list">
            <a href="https://www.ecoledirecte.com/login" class="about-link" target="_blank">École Direct</a>
            <p>Par ce biais, vous pourrez consulter vos notes, moyennes, emploi du temps, agenda, messages.</p>
        </li>
        <li class="about-list">
            <a href="https://folios.onisep.fr/saml/login?_saml_idp=" class="about-link" target="_blank">Folios</a>
            <p>Vous aidera à conserver ce que vous avez appris durant votre cursus, pour vous préparer à la suite de vos études.</p>
        </li>
        <li class="about-list">
            <a href="https://www.e-interforum.com/cas/?service=http://portail.cns-edu.com/" class="about-link" target="_blank">CNS</a> et
            <a href="https://educadhoc.fr/" class="about-link" target="_blank">Educhadoc</a>
            <p>Vos portails de livres numériques.</p>
        </li>
        <li class="about-list">
            <a href="http://www.notre-dame-reze.fr/eleves/pre-convention-de-stage" class="about-link">Pré-convention de stage</a>
            <p>Afin de télécharger votre pré-convention de stage.</p>
        </li>
        <li class="about-list">
            <a href="https://www.cerise-pro.fr/0440274J/" class="about-link" target="_blank">Cerise Pro</a>
            <p>Pour vous permettre de constituer votre passeport professionnel.</p>
        </li>

    </ul>
</div>
@endsection