@guest
@else
<!-- Footer -->
<footer class="footer">
    <div class="container">
        {{--<div class="col-md-3 mb-md-0 mb-3">--}}

            <!-- Links -->
            <h5 class="text-uppercase">Liens utiles</h5>

            <ul class="list-unstyled">
                <div class="row">
                    <div id="links" class="col">
                        <li>
                            <a href="/about">À Propos</a>
                        </li>
                        <li>
                            <a href="http://notre-dame-reze.fr/accueil/mentions-legales">Mentions Légales</a>
                        </li>
                    </div>




                    <div id="icons" class="col">
                        <li><a href="https://www.facebook.com/Lyc%C3%A9e-Notre-Dame-de-Rez%C3%A9-348337121874194/" target="_blank"><img src="{{ asset('img/facebookpicto.jpg') }}" class="footer-img"></a></li>
                        <li><a href="https://twitter.com/LyceesNotreDame" target="_blank"><img src="{{ asset('img/twitterpicto.jpg') }}" class="footer-img"></a></li>
                        <li><a href="https://www.linkedin.com/company/2883407?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A3850239921466424738436%2CVSRPtargetId%3A2883407%2CVSRPcmpt%3Aprimary" target="_blank"><img src="{{ asset('img/linkedinpicto.jpg') }}" class="footer-img"></a></li>

                    </div>

                </div>
            </ul>



        </div>
        <div id="copyright">
            © Ensemble Scolaire Notre-Dame / Saint-Paul
        </div>
    </div>
</footer>
<!-- Footer -->
@endguest